<?php

use App\Http\Controllers\TranscationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VisitorController;
use App\Http\Controllers\VoucherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return "FIND API IS RUNNING!";
});

Route::get('/users', [UserController::class, "auth"]);
Route::get('/users/key/{key}', [UserController::class, "getData"]);
Route::get('/users/{id}/key/{key}', [UserController::class, "getDataById"]);
Route::post('/users', [UserController::class, "postData"]);
Route::post('/users/update', [UserController::class, "putData"]);
Route::get('/users/delete/{id}/key/{key}', [UserController::class, "deleteData"]);

Route::get('/vouchers/key/{key}', [VoucherController::class, "getData"]);
Route::post('/vouchers', [VoucherController::class, "postData"]);
Route::post('/vouchers/update', [VoucherController::class, "updateData"]);
Route::get('/vouchers/{id}/key/{key}', [VoucherController::class, "getDataById"]);
Route::get('/vouchers/type/{type}/key/{key}', [VoucherController::class, "getDataByType"]);
Route::get('/vouchers/delete/{id}/key/{key}', [VoucherController::class, "deleteData"]);

Route::get('/transactions/key/{key}', [TranscationController::class, "getTransactions"]);
Route::get('/transactions/key/{key}/user/{id}', [TranscationController::class, "getTransactionsByUserId"]);
Route::post('/transactions', [TranscationController::class, "postData"]);
Route::get('/transactions/delete/{id}/key/{key}', [TranscationController::class, "deleteData"]);
Route::get('/transactions/{id}/key/{key}', [TranscationController::class, "getTransactionsById"]);

Route::get('/visitor/key/{key}', [VisitorController::class, "getVisitors"]);
Route::post('/visitor', [VisitorController::class, "postVisitors"]);
Route::get('/visitor/delete/{id}/key/{key}', [VisitorController::class, "deleteVisitors"]);
Route::get('/visitor/key/{key}/all', [VisitorController::class, "countVisitors"]);
