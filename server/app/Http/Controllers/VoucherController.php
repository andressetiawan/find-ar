<?php

namespace App\Http\Controllers;

use App\Models\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function getData($key = null)
    {
        return ($key !== env('API_KEY')) ?
            ["result" => "unauthorized access"]
            : Voucher::all();
    }

    public function postData(Request $req)
    {
        if ($req->key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }
        $voucher = new Voucher();
        $voucher->name = $req->name;
        $voucher->company = $req->company;
        $voucher->description = $req->description;
        $result = $voucher->save();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }

    public function getDataById($id = null, $key = null)
    {
        return ($key !== env('API_KEY')) ?
            ["result" => "unauthorized access"]
            : Voucher::find($id);
    }

    public function updateData(Request $req)
    {
        if ($req->key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }

        $voucher = Voucher::where('id', '=', $req->id)->where('company', '=', $req->company)->first();
        if ($voucher == null) {
            return ["result" => "Not found"];
        }
        $voucher->name = $req->name;
        $voucher->description = $req->description;
        $result = $voucher->save();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }

    public function deleteData($id = null, $key = null)
    {
        if ($key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }

        $voucher = Voucher::find($id);
        $result = $voucher->delete();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }
}
