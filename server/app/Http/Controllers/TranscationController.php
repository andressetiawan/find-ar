<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TranscationController extends Controller
{
    public function getTransactions($key)
    {
        return ($key !== env("API_KEY")) ? ["result" => "unauthorized access"] : Transaction::all();
    }

    public function postData(Request $req)
    {
        if ($req->key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }
        $transaction = new Transaction();
        $transaction->user_id = $req->user_id;
        $transaction->voucher_id = $req->voucher_id;
        $result = $transaction->save();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }

    public function deleteData($id, $key)
    {
        if ($key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }

        $transaction = Transaction::find($id);
        $result = $transaction->delete();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }

    public function getTransactionsByUserId($key, $id)
    {
        if ($key !== env("API_KEY")) {
            return ["result" => "unauthorized access"];
        } else {
            return Transaction::join("users", "users.id", "=", "transactions.user_id")
                ->join("vouchers", "vouchers.id", "=", "transactions.voucher_id")
                ->select("transactions.id", "transactions.voucher_id", "vouchers.name", "vouchers.company", "vouchers.description", "transactions.created_at")
                ->where("transactions.user_id", "=", $id)->orderBy("transactions.id", "DESC")->get();
        }
    }

    public function getTransactionsById($id, $key)
    {
        if ($key !== env("API_KEY")) {
            return ["result" => "unauthorized access"];
        } else {
            return Transaction::join("users", "users.id", "=", "transactions.user_id")
                ->join("vouchers", "vouchers.id", "=", "transactions.voucher_id")
                ->select("transactions.id", "transactions.voucher_id", "vouchers.name", "vouchers.type", "vouchers.company", "vouchers.description", "transactions.created_at")
                ->where("transactions.id", "=", $id)->get();
        }
    }
}
