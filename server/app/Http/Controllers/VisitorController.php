<?php

namespace App\Http\Controllers;

use App\Models\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitorController extends Controller
{
    public function getVisitors($key = null)
    {
        return ($key == env("API_KEY") ? Visitor::all() : ["result" => "unauthorized access"]);
    }

    public function postVisitors(Request $req)
    {
        if ($req->key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }
        $visitor = new Visitor();
        $visitor->user_id = $req->user_id;
        $visitor->place = $req->place;
        $result = $visitor->save();

        return ($result ? ["result" => "success"] : ["result" => "failed"]);
    }

    public function deleteVisitors($id = null, $key = null)
    {
        if ($key != env("API_KEY")) {
            return ["result" => "unauthorized access"];
        }
        $visitor = new Visitor();
        $result = $visitor->find($id)->delete();
        return ($result ? ["result" => "success"] : ["result" => "failed"]);
    }

    public function countVisitors($key = null)
    {
        if ($key != env("API_KEY")) {
            return ["result" => "unauthorized access"];
        }
        $visitor = new Visitor();
        $result = Visitor::select('place', DB::raw("COUNT(user_id) as total_visitor"))->groupBy('place')->orderBy("total_visitor", "DESC")->get();
        return $result;
    }
}
