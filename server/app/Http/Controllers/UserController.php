<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getData($key = null)
    {
        return ($key !== env('API_KEY')) ?
            ["result" => "unauthorized access"]
            : User::all();
    }

    public function auth()
    {
        return ["result" => "unauthorized access"];
    }

    public function getDataById($id = null, $key = null)
    {
        return ($key !== env('API_KEY')) ?
            ["result" => "unauthorized access"]
            : User::find($id);
    }

    public function postData(Request $req)
    {
        if ($req->key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }

        $user = new User();
        $user->id = $req->id;
        $user->first_name = $req->first_name;
        $user->last_name = $req->last_name;
        $result = $user->save();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }

    public function putData(Request $req)
    {
        if ($req->key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }

        $user = User::find($req->id);
        $user->coins = $req->coins;

        $result = $user->save();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }

    public function deleteData($id = null, $key = null)
    {
        if ($key !== env('API_KEY')) {
            return ["result" => "unauthorized access"];
        }

        $user = User::find($id);
        $result = $user->delete();

        return $result ? ["result" => "Success"] : ["result" => "Failed"];
    }
}
