using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quiz : MonoBehaviour
{
    public GameObject[] prefabQuiz;
    public Text[] choicesPanel;
    public string[] choices;
    public Answer[] answers;
    public int benar;

    public void QuizStart()
    {
        int randomQueston = Random.Range(0, prefabQuiz.Length);
        int randomPanel = Random.Range(0, choicesPanel.Length);
        answers[randomPanel].answer = true;
        int randomAnotherQuestion1 = 0;
        int randomAnotherQuestion2 = 0;
        int randomAnotherQuestion3 = 0;

        choicesPanel[randomPanel].text = choices[randomQueston];
        GameObject item = Instantiate(prefabQuiz[randomQueston],
                new Vector3(200, 900, -400),
                prefabQuiz[randomQueston].transform.rotation) as GameObject;

        int j = 0;
        for (int i = 0; i <= 3; i++)
        {
            if (i != randomPanel)
            {
                answers[i].answer = false;
                j++;
                if (j == 1)
                {
                    do
                    {
                        randomAnotherQuestion1 = Random.Range(0, prefabQuiz.Length);
                        choicesPanel[i].text = choices[randomAnotherQuestion1];
                    } while (randomAnotherQuestion1 == randomQueston);
                }
                else if (j == 2)
                {
                    do
                    {
                        randomAnotherQuestion2 = Random.Range(0, prefabQuiz.Length);
                        choicesPanel[i].text = choices[randomAnotherQuestion2];
                    } while (randomAnotherQuestion2 == randomAnotherQuestion1 || randomAnotherQuestion2 == randomQueston);
                }
                else if (j == 3)
                {
                    do
                    {
                        randomAnotherQuestion3 = Random.Range(0, prefabQuiz.Length);
                        choicesPanel[i].text = choices[randomAnotherQuestion3];
                    } while (randomAnotherQuestion3 == randomQueston || randomAnotherQuestion3 == randomAnotherQuestion1 || randomAnotherQuestion3 == randomAnotherQuestion2);
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        benar = 0;
        QuizStart();
    }

}
