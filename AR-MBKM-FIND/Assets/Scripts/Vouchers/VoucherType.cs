﻿using UnityEngine;
public class VoucherType : MonoBehaviour
{
  public static string FOOD_DISCOUNT = "food_discount";
  public static string FOOD_PRICE = "food_price";
  public static string OTHER_DISCOUNT = "other_discount";
  public static string OTHER_PRICE = "other_price";
}
