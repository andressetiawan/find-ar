using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheWaySpawner : MonoBehaviour
{
    // void Start()
    // {
        // Sistem.instance.WayOneToTwo.SetActive(false);
        // Sistem.instance.WayOneToThree.SetActive(false);
        // Sistem.instance.WayOneToFour.SetActive(false);
        // Sistem.instance.WayOneToFive.SetActive(false);

        // Sistem.instance.WayTwoToOne.SetActive(false);
        // Sistem.instance.WayTwoToThree.SetActive(false);
        // Sistem.instance.WayTwoToFour.SetActive(false);
        // Sistem.instance.WayTwoToFive.SetActive(false);

        // Sistem.instance.WayThreeToOne.SetActive(false);
        // Sistem.instance.WayThreeToTwo.SetActive(false);
        // Sistem.instance.WayThreeToFour.SetActive(false);
        // Sistem.instance.WayThreeToFive.SetActive(false);

        // Sistem.instance.WayFourToOne.SetActive(false);
        // Sistem.instance.WayFourToTwo.SetActive(false);
        // Sistem.instance.WayFourToThree.SetActive(false);
        // Sistem.instance.WayFourToFive.SetActive(false);

        // Sistem.instance.WayFiveToOne.SetActive(false);
        // Sistem.instance.WayFiveToTwo.SetActive(false);
        // Sistem.instance.WayFiveToThree.SetActive(false);
        // Sistem.instance.WayFiveToFour.SetActive(false);
    // }

    // ------------- ONE -------------------------------------------
    public void SpawnWayOneToTwo()
    {
        Sistem.instance.WayOneToTwo.SetActive(true);

        // Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayOneToThree()
    {
        Sistem.instance.WayOneToThree.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        // Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayOneToFour()
    {
        Sistem.instance.WayOneToFour.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        // Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayOneToFive()
    {
        Sistem.instance.WayOneToFive.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        // Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    // ------------- TWO ---------------------------------------------------

    public void SpawnWayTwoToOne()
    {
        Sistem.instance.WayTwoToOne.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        // Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayTwoToThree()
    {
        Sistem.instance.WayTwoToThree.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        // Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayTwoToFour()
    {
        Sistem.instance.WayTwoToFour.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        // Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayTwoToFive()
    {
        Sistem.instance.WayTwoToFive.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        // Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    // ------------- THREE ---------------------------------------------------

    public void SpawnWayThreeToOne()
    {
        Sistem.instance.WayThreeToOne.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        // Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayThreeToTwo()
    {
        Sistem.instance.WayThreeToTwo.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        // Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayThreeToFour()
    {
        Sistem.instance.WayThreeToFour.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        // Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayThreeToFive()
    {
        Sistem.instance.WayThreeToFive.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        // Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    // ------------- FOUR ---------------------------------------------------

    public void SpawnWayFourToOne()
    {
        Sistem.instance.WayFourToOne.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        // Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayFourToTwo()
    {
        Sistem.instance.WayFourToTwo.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        // Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayFourToThree()
    {
        Sistem.instance.WayFourToThree.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        // Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayFourToFive()
    {
        Sistem.instance.WayFourToFive.SetActive(true);

        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        // Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    // ------------- FIVE ---------------------------------------------------

    public void SpawnWayFiveToOne()
    {
        Sistem.instance.WayFiveToOne.SetActive(true);

        // --- Hide From One ---
        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        // --- Hide From Two ---
        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        // --- Hide From Three ---
        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        // --- Hide From Four ---
        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        // --- Hide From Five ---
        // Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayFiveToTwo()
    {
        Sistem.instance.WayFiveToTwo.SetActive(true);

        // --- Hide From One ---
        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        // --- Hide From Two ---
        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        // --- Hide From Three ---
        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        // --- Hide From Four ---
        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        // --- Hide From Five ---
        Sistem.instance.WayFiveToOne.SetActive(false);
        // Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayFiveToThree()
    {
        Sistem.instance.WayFiveToThree.SetActive(true);

        // --- Hide From One ---
        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        // --- Hide From Two ---
        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        // --- Hide From Three ---
        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        // --- Hide From Four ---
        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        // --- Hide From Five ---
        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        // Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

    public void SpawnWayFiveToFour()
    {
        Sistem.instance.WayFiveToFour.SetActive(true);

        // --- Hide From One ---
        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        // --- Hide From Two ---
        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        // --- Hide From Three ---
        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        // --- Hide From Four ---
        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        // --- Hide From Five ---
        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        // Sistem.instance.WayFiveToFour.SetActive(false);
    }


    // ----------- Hide All Way ---------------
    public void HideAllWay() 
    {
        Sistem.instance.WayOneToTwo.SetActive(false);
        Sistem.instance.WayOneToThree.SetActive(false);
        Sistem.instance.WayOneToFour.SetActive(false);
        Sistem.instance.WayOneToFive.SetActive(false);

        Sistem.instance.WayTwoToOne.SetActive(false);
        Sistem.instance.WayTwoToThree.SetActive(false);
        Sistem.instance.WayTwoToFour.SetActive(false);
        Sistem.instance.WayTwoToFive.SetActive(false);

        Sistem.instance.WayThreeToOne.SetActive(false);
        Sistem.instance.WayThreeToTwo.SetActive(false);
        Sistem.instance.WayThreeToFour.SetActive(false);
        Sistem.instance.WayThreeToFive.SetActive(false);

        Sistem.instance.WayFourToOne.SetActive(false);
        Sistem.instance.WayFourToTwo.SetActive(false);
        Sistem.instance.WayFourToThree.SetActive(false);
        Sistem.instance.WayFourToFive.SetActive(false);

        Sistem.instance.WayFiveToOne.SetActive(false);
        Sistem.instance.WayFiveToTwo.SetActive(false);
        Sistem.instance.WayFiveToThree.SetActive(false);
        Sistem.instance.WayFiveToFour.SetActive(false);
    }

}
