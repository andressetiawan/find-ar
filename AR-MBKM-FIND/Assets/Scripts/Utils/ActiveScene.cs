using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ActiveScene : MonoBehaviour
{

  void Start()
  {
    switch (SceneManager.GetActiveScene().buildIndex)
    {
      case 1:
        ActiveNavbar("Home");
        break;

      case 2:
        ActiveNavbar("Nav");
        break;

      case 3:
        ActiveNavbar("Reward");
        break;

      case 4:
        ActiveNavbar("Setting");
        break;

      case 5:
        ActiveNavbar("Scan");
        break;

      case 6:
        ActiveNavbar("Reward");
        break;

      case 8:
        ActiveNavbar("Reward");
        break;

      default:
        ActiveNavbar("Home");
        break;
    }
  }

  void ActiveNavbar(string objName)
  {
    Image navIcon = GameObject.Find(objName).GetComponent<Image>();
    Color active = navIcon.color;
    active.a = 1f;
    navIcon.color = active;
  }
}
